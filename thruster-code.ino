/*

*/
#include "Servo.h"

Servo front_right_thruster;
Servo front_left_thruster;
Servo rear_right_thruster;
Servo rear_left_thruster;


int right_joystick_x_pin = A0;
int right_joystick_y_pin = A1;
int left_joystick_x_pin = A2;
int left_joystick_y_pin = A3;

int right_joystick_x_value = 0;
int right_joystick_y_value = 0;
int left_joystick_x_value = 0;
int left_joystick_y_value = 0;

int left_joystick_x_deadzone = 512;
int left_joystick_y_deadzone = 512;
int right_joystick_x_deadzone = 512;
int right_joystick_y_deadzone = 512;

int deadzone_tolerance = 20;

void setup() {
  Serial.begin(9600);
  
  pinMode(right_joystick_x_pin, INPUT);
  pinMode(right_joystick_y_pin, INPUT);
  pinMode(left_joystick_x_pin, INPUT);
  pinMode(left_joystick_y_pin, INPUT);
  
  front_right_thruster.attach(3, 1100, 1900);
  front_left_thruster.attach(5, 1100, 1900);
  rear_right_thruster.attach(6, 1100, 1900);
  rear_left_thruster.attach(9, 1100, 1900);
  
  front_right_thruster.writeMicroseconds(1500); // Initalize ESC with stop command
  front_left_thruster.writeMicroseconds(1500);  
  rear_right_thruster.writeMicroseconds(1500);
  rear_left_thruster.writeMicroseconds(1500);
  
  left_joystick_x_deadzone = analogRead(left_joystick_x_pin);
  left_joystick_y_deadzone = analogRead(left_joystick_y_pin);
  right_joystick_x_deadzone = analogRead(right_joystick_x_pin);
  right_joystick_y_deadzone = analogRead(right_joystick_y_pin);
  
  
  delay(7000);  // Delay to allow ESCs to boot up
  
  
  Serial.println("System booted");
}

void loop() {
    right_joystick_x_value = analogRead(right_joystick_x_pin);
    right_joystick_y_value = analogRead(right_joystick_y_pin);
    left_joystick_x_value = analogRead(left_joystick_x_pin);
    left_joystick_y_value = analogRead(left_joystick_y_pin);
    
    if (right_joystick_x_value > right_joystick_x_deadzone + deadzone_tolerance) {
      front_right_thruster.writeMicroseconds(map(right_joystick_x_value, right_joystick_x_deadzone, 1024, 1500, 1900));
    }
    else if (right_joystick_x_value < right_joystick_x_deadzone - deadzone_tolerance) {
      front_right_thruster.writeMicroseconds(map(right_joystick_x_value, right_joystick_x_deadzone, 0, 1500, 1100));
    }
    else {
      front_right_thruster.writeMicroseconds(1500);
    }
    
    if (left_joystick_x_value > left_joystick_x_deadzone + deadzone_tolerance) {
      front_left_thruster.writeMicroseconds(map(left_joystick_x_value, left_joystick_x_deadzone, 1024, 1500, 1900));
    }
    else if (left_joystick_x_value < left_joystick_x_deadzone - deadzone_tolerance) {
      front_left_thruster.writeMicroseconds(map(left_joystick_x_value, left_joystick_x_deadzone, 0, 1500, 1100));
    }
    else {
      front_left_thruster.writeMicroseconds(1500);
    }

    if (right_joystick_y_value > right_joystick_y_deadzone + deadzone_tolerance) {
      rear_right_thruster.writeMicroseconds(map(right_joystick_y_value, right_joystick_y_deadzone, 1024, 1500, 1900));
    }
    else if (right_joystick_y_value < right_joystick_y_deadzone - deadzone_tolerance) {
      rear_right_thruster.writeMicroseconds(map(right_joystick_y_value, right_joystick_y_deadzone, 0, 1500, 1100));
    }
    else {
      rear_right_thruster.writeMicroseconds(1500);
    }
    
    if (left_joystick_y_value > left_joystick_y_deadzone + deadzone_tolerance) {
      rear_left_thruster.writeMicroseconds(map(left_joystick_y_value, left_joystick_y_deadzone, 1024, 1500, 1900));
    }
    else if (left_joystick_y_value < left_joystick_y_deadzone - deadzone_tolerance) {
      rear_left_thruster.writeMicroseconds(map(left_joystick_y_value, left_joystick_y_deadzone, 0, 1500, 1100));
    }
    else {
      rear_left_thruster.writeMicroseconds(1500);
    }
    
}
